/*
 * MKL25Z4 SDHC Card Driver
 *
 * Copyright (c) 2015 SHA Miao
 * This program is licensed under [The MIT License](http://opensource.org/licenses/MIT).
 */

#include <stdint.h>
#include "MKL25Z4.h"
#include "sdhccard.h"
#include "gpio.h"
#include "spi1.h"

#define SDHC_COMMAND_MASK ((uint8_t)(0x3FU))
#define SDHC_BLOCK_SIZE   512

void sdhc_delay(unsigned int ubb) {
  // 4000 per 1ms (approx.) under 48MHz core clock.
  unsigned int iDelay;
  for (iDelay=0; iDelay<ubb; iDelay++) { asm("nop"); }
}

inline void sdhc_start_transaction() { digitalWrite(PORTB, 10, LOW); }
inline void sdhc_end_transaction()   { digitalWrite(PORTB, 10, HIGH); }

void sdhc_command(uint8_t cmd, uint32_t arg, uint8_t crc, int n_resp, uint8_t *resp)
{
  int i;
  uint8_t arg_bytes[4];
  for (i=0; i<4; i++) { arg_bytes[i] = (uint8_t)(arg >> (i*8)); }

  spi1_write(0x40U | (cmd & SDHC_COMMAND_MASK)); // Command
  for (i=0; i<4; i++) { spi1_write(arg_bytes[4 - i - 1]); } // Argument
  spi1_write(crc << 1 | 0x01); // CRC

  if (cmd == 12) // For STOP_TRANSMISSION command (R1b response)
  {
    spi1_write(0xFF); // discard the byte immediately read
    while ( (resp[0] = spi1_write(0xFF)) == 0xFF ) { ; } // Wait for R1 response
    //TODO: timeout check w/ systick
    while ( spi1_write(0xFF) != 0xFF ) { ; } // Wait until busy (MOSI pulled low) over
    //TODO: timeout check w/ systick
  }
  else
  {
    if (n_resp == 0) { return; }
    while ( (resp[0] = spi1_write(0xFF)) == 0xFF ) { ; }
    //TODO: timeout check w/ systick
    for (i=1; i<n_resp; i++) {
      resp[i] = spi1_write(0xFF);
    }
  }
}

void sdhc_init()
{
  int i;
  uint8_t buf[12];

  spi1_init();
  // Disable MCU native /SS output and set /SS pin as GPIO.
  // (Since SD card requires /CS stay low during a complete transaction)
  SPI1_C2 &= ~SPI_C2_MODFEN_MASK;
  pinMode(PORTB, 10, OUTPUT);
  pinMux(PORTB, 10, MUX1_GPIO);
  digitalWrite(PORTB, 10, HIGH);

  // Power on, Wait for 1ms+
  sdhc_delay(6000);
  // Set a clock rate between 100k and 400kHz
  // ( Bus clock=24MHz * SPPR(2)=1/3 * SPR(5)=1/64 ) = 125kHz
  SPI1_BR &= ~SPI_BR_SPPR_MASK; SPI1_BR |= SPI_BR_SPPR(2);
  SPI1_BR &= ~SPI_BR_SPR_MASK;  SPI1_BR |= SPI_BR_SPR(5);
  // Send 74+ SCK pulses with MOSI and CS high
  digitalWrite(PORTB, 10, HIGH);
  for (i=0; i<10; i++) { spi1_write(0xFF); }

  // Go to SPI mode
  sdhc_start_transaction();
  sdhc_command(0, 0x00000000U, 0x4A, 1, buf); // SD mode CMD0 + /CS LOW = Go to SPI mode
  sdhc_end_transaction();
  // TODO: check response 0x01

  sdhc_start_transaction();
  sdhc_command(8, 0x000001AAU, 0x43, 9, buf); // CMD8(SEND_IF_COND)
  sdhc_end_transaction();
  // TODO: check response 0x000001AAU

  while (1) {
    sdhc_start_transaction();
    sdhc_command(55, 0x00000000U, 0x32, 1, buf); // CMD55(APP_CMD)
    sdhc_end_transaction();

    sdhc_start_transaction();
    sdhc_command(41, 0x40000000U, 0x62, 1, buf); // ACMD41(APP_SEND_OP_COND)
    sdhc_end_transaction();

    if (buf[0] == 0x00) { break; }
  }
  // TODO: check response 0x00

  sdhc_start_transaction();
  sdhc_command(58, 0x00000000U, 0x7E, 9, buf); // CMD58(READ_OCR)
  sdhc_end_transaction();
  // TODO: check response 0x40000000

  sdhc_start_transaction();
  sdhc_command(58, 0x00000000U, 0x7E, 9, buf); // CMD58(READ_OCR)
  sdhc_end_transaction();
  // TODO: check response 0x40000000

  sdhc_start_transaction();
  sdhc_command(59, 0x00000000U, 0x01, 1, buf); // CMD59(CRC_ON_OFF)
  sdhc_end_transaction();
  // TODO: check response 0x00

  // Set a higher clock rate for data transmission
  // ( Bus clock=24MHz * SPPR(2)=1/3 * SPR(0)=1/2 ) = 4MHz
  SPI1_BR &= ~SPI_BR_SPPR_MASK; SPI1_BR |= SPI_BR_SPPR(2);
  SPI1_BR &= ~SPI_BR_SPR_MASK;  SPI1_BR |= SPI_BR_SPR(0);
}

void sdhc_read_blocks(uint32_t addr, int n_blocks, uint8_t *data)
{
  int i, iblk;
  uint8_t buf[128];

  sdhc_start_transaction();
  sdhc_command(18, addr, 0xFF, 1, buf); // CMD59(CRC_ON_OFF)
  // TODO: check data response 0x05

  for (iblk=0; iblk<n_blocks; iblk++)
  {
    while ( (buf[0] = spi1_write(0xFF)) == 0xFF) { ; } // Data token
    // TODO: timeout check w/ systick
    // TODO: check data token 0xFE

    for (i=0; i<SDHC_BLOCK_SIZE; i++) {
      *(data++) = spi1_write(0xFF);
    }

    spi1_write(0xFF); spi1_write(0xFF); // CRC bytes (discard)
  }

  sdhc_command(12, 0x00000000U, 0xFF, 1, buf);
  sdhc_end_transaction();
}

