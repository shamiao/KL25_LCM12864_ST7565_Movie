/*
 * MKL25Z4 ST7565 128x64 LCD Module Driver
 *
 * Copyright (c) 2015 SHA Miao
 * This program is licensed under [The MIT License](http://opensource.org/licenses/MIT).
 */

#include <stdint.h>
#include "MKL25Z4.h"
#include "gpio.h"
#include "spi0.h"
#include "lcm12864_st7565.h"

const uint8_t lcm_init_cmds[] = {
	0xAF, 0x40, 0xA0, 0xA6, 0xA4, 0xA2, 0xC8, 0x2F,	0x24, 0x81, 0x24, 0xFF,
};

void lcm_delay() {
  // 4000 per 1ms (approx.) under 48MHz core clock.
  unsigned int iDelay;
  for (iDelay=0; iDelay<400; iDelay++) { asm("nop"); }
}

void lcm_init()
{
	const uint8_t *lcm_init_cmd;

	// Init SPI0 (LCM12864 Data)
	spi0_init();

	// Init PTA13 (LCM12864 /RST)
	pinMux(0, 13, MUX1_GPIO);
	pinMode(0, 13, OUTPUT);
	digitalWrite(0, 13, HIGH);

	// Init PTA12 (LCM12864 A0)
  pinMux(0, 12, MUX1_GPIO);
  pinMode(0, 12, OUTPUT);

	// Send reset pulse
  digitalWrite(0, 13, HIGH);
  lcm_delay();
  digitalWrite(0, 13, LOW);
  lcm_delay();
  digitalWrite(0, 13, HIGH);
  lcm_delay();

	// Send init commands
	lcm_init_cmd = lcm_init_cmds;
  digitalWrite(0, 12, LOW); // L=Command
  lcm_delay();
	while (*lcm_init_cmd != 0xFF) {
		spi0_write(*(lcm_init_cmd++));
	}

	lcm_clear();
}

void lcm_clear()
{
	unsigned int iPage, iPos;

	for (iPage=0; iPage<8; iPage++) {
	  lcm_delay();
	  digitalWrite(0, 12, LOW); // L=Command
	  lcm_delay();
		spi0_write(0xB0 + iPage);
		spi0_write(0x10);
		spi0_write(0x00);
	  lcm_delay();
	  digitalWrite(0, 12, HIGH); // H=Data
	  lcm_delay();
		for (iPos=0; iPos<128; iPos++) {
			spi0_write(0x00);
		}
	}
}

void lcm_bitmap(uint8_t *bmp)
{
	unsigned int iPage, iPos;

	for (iPage=0; iPage<8; iPage++) {
	  lcm_delay();
    digitalWrite(0, 12, LOW); // L=Command
    lcm_delay();
		spi0_write(0xB0 + iPage);
		spi0_write(0x10);
		spi0_write(0x00);
	  lcm_delay();
    digitalWrite(0, 12, HIGH); // H=Data
    lcm_delay();
		for (iPos=0; iPos<128; iPos++) {
			spi0_write(*(bmp++));
		}
    lcm_delay();
	}
}
