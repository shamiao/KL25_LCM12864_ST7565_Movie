/*
 * MKL25Z4 SDHC Card Driver
 *
 * Copyright (c) 2015 SHA Miao
 * This program is licensed under [The MIT License](http://opensource.org/licenses/MIT).
 */

#ifndef SDHCCARD_H_
#define SDHCCARD_H_

#include <stdint.h>

#define SDHC_SECTOR_SIZE 512

void sdhc_init();

void sdhc_read_blocks(uint32_t addr, int n_blocks, uint8_t *data);

#endif /* SDHCCARD_H_ */
