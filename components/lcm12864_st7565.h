/*
 * MKL25Z4 ST7565 128x64 LCD Module Driver
 *
 * Copyright (c) 2015 SHA Miao
 * This program is licensed under [The MIT License](http://opensource.org/licenses/MIT).
 */

#ifndef LCM12864_ST7565_H_
#define LCM12864_ST7565_H_

#include <stdint.h>

void lcm_init();

void lcm_clear();

void lcm_bitmap(uint8_t *bmp);

#endif /* LCM12864_ST7565_H_ */
