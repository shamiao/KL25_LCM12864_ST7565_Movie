/*
 * ST7565 Movie Demonstration for MKL25Z4
 *
 * Copyright (c) 2015 SHA Miao
 * This program is licensed under [The MIT License](http://opensource.org/licenses/MIT).
 */

#include "common.h"
#include <stdint.h>
#include "systick.h"
#include "gpio.h"
#include "sdhccard.h"
#include "lcm12864_st7565.h"

#define FRAMES     6565
#define MILLI_FPS  29970
#define FRAME_SIZE 1024
#define SD_SECTORS_PER_FRAME (FRAME_SIZE / SDHC_SECTOR_SIZE)

uint32_t g_tick_ms;

uint8_t buff_bmp[1024];

void t_delay(unsigned int ubb) {
  // 4000 per 1ms (approx.) under 48MHz core clock.
  unsigned int iDelay;
  for (iDelay=0; iDelay<ubb; iDelay++) { asm("nop"); }
}

int duration(int milli_fps, int frames)
{
  int64_t r = 1000;
  r *= frames;
  r *= 1000;
  r /= milli_fps;
  return (int)r;
}

int main(void)
{
  int total_play_time;
  int prev_frame, curr_frame;
  uint32_t start_at;

  // Initialization
	DISABLE_INTERRUPTS;

	// Calculate movie time related data
  total_play_time = duration(MILLI_FPS, FRAMES);

  // Setup debug signal output
  pinMode(1, 18, OUTPUT);
  digitalWrite(1, 18, HIGH);
  pinMux(1, 18, MUX1_GPIO);

  // Setup components
  sdhc_init();
  lcm_init();

  // Setup system-wide timer
	systick_init();
  g_tick_ms = 0;
	systick_enable();

  ENABLE_INTERRUPTS;

  // Main loop
	prev_frame = -1;
	start_at = g_tick_ms;
	while (1) {
	  // Real meaning is (g_tick_ms / total_play_time) * total_frames.
	  curr_frame = (g_tick_ms - start_at) * FRAMES / total_play_time;
	  if (curr_frame == prev_frame) { continue; }
	  if (curr_frame > FRAMES) { break; }

	  digitalWrite(1, 18, LOW); // Indicates the start of data transmission of 1 frame
    sdhc_read_blocks(curr_frame * SD_SECTORS_PER_FRAME, SD_SECTORS_PER_FRAME, buff_bmp);
    lcm_bitmap((uint8_t *)(buff_bmp));
    digitalWrite(1, 18, HIGH); // Indicates the end of data transmission of 1 frame

    prev_frame = curr_frame;
	}

  systick_disable();
  DISABLE_INTERRUPTS;
  while (1) { ; }
	return 0;
}


