/*
 * MKL25Z4 SysTick Driver
 *
 * Copyright (c) 2015 SHA Miao
 * This program is licensed under [The MIT License](http://opensource.org/licenses/MIT).
 */

#include "MKL25Z4.h"
#include "systick.h"

void systick_init()
{
  // Clear existing settings
  SYST_CSR = 0;
  SYST_CVR = 0;

  SYST_CSR |= 1U << SysTick_CSR_CLKSOURCE_SHIFT; // Kernel clock = 48MHz
  SYST_RVR = 48000; // 1 / 48MHz * 48000ticks = 1ms
  SYST_CSR |= 1U << SysTick_CSR_TICKINT_SHIFT; // Enable Interrupt
}

inline void systick_enable() {
  SYST_CSR |= 1U << SysTick_CSR_ENABLE_SHIFT;
}

inline void systick_disable() {
  SYST_CSR &= ~SysTick_CSR_ENABLE_MASK;
}
