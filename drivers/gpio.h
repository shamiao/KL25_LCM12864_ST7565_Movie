/*
 * MKL25Z4 GPIO Driver (Arduino Style)
 *
 * Copyright (c) 2015 SHA Miao
 * This program is licensed under [The MIT License](http://opensource.org/licenses/MIT).
 */

#ifndef GPIO_H_
#define GPIO_H_

#include <stdint.h>

#define PORTA 0
#define PORTB 1
#define PORTC 2
#define PORTD 3
#define PORTE 4

#define HIGH 0x1U
#define LOW  0x0U

#define INPUT  0x0U
#define OUTPUT 0x1U

#define MUX0_DFLT 0
#define MUX1_GPIO 1
#define MUX2_COMM 2
#define MUX3 3
#define MUX4 4
#define MUX5 5
#define MUX6 6
#define MUX7 7

void pinMux(uint8_t port, uint8_t pin, uint8_t mux);
void pinMode(uint8_t port, uint8_t pin, uint8_t mode);
void digitalWrite(uint8_t port, uint8_t pin, uint8_t value);
int8_t digitalRead(uint8_t port, uint8_t pin);
void digitalFlip(uint8_t port, uint8_t pin);

#endif /* GPIO_H_ */
