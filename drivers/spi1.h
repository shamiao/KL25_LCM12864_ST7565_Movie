/*
 * MKL25Z4 SPI1 Driver
 *
 * Copyright (c) 2015 SHA Miao
 * This program is licensed under [The MIT License](http://opensource.org/licenses/MIT).
 */

#ifndef SPI1_H_
#define SPI1_H_

#include <stdint.h>

void spi1_init();

uint8_t spi1_write(uint8_t data);

#endif /* SPI1_H_ */
