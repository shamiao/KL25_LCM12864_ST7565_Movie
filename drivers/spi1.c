/*
 * MKL25Z4 SPI1 Driver
 *
 * Copyright (c) 2015 SHA Miao
 * This program is licensed under [The MIT License](http://opensource.org/licenses/MIT).
 */

#include <stdint.h>
#include "MKL25Z4.h"
#include "gpio.h"
#include "spi1.h"

void spi1_init()
{
	// Enable SPI Clock
	SIM_SCGC4 |= (1U << SIM_SCGC4_SPI1_SHIFT);
	// PTB10 = ALT2: SPI1_PCS0
	pinMux(PORTB, 10, MUX2_COMM);
	// PTB11 = ALT2: SPI1_SCK
	pinMux(PORTB, 11, MUX2_COMM);
	// PTB16 = ALT2: SPI1_MOSI
  pinMux(PORTB, 16, MUX2_COMM);
	// PTB17 = ALT2: SPI1_MISO
  pinMux(PORTB, 17, MUX2_COMM);
	// SPI Settings: Master, CPOL=0 CPHA=0 (SPI Mode 0), LSBFE=0 (MSB First)
	SPI1_C1 |= (1U << SPI_C1_MSTR_SHIFT);
  SPI1_C1 &= ~SPI_C1_CPOL_MASK;
  SPI1_C1 &= ~SPI_C1_CPHA_MASK;
	SPI1_C1 &= ~SPI_C1_LSBFE_MASK;
	// SPI Settings: /SS Port as /CS Output
	SPI1_C1 |= (1U << SPI_C1_SSOE_SHIFT);
	SPI1_C2 |= (1U << SPI_C2_MODFEN_SHIFT);
	// SPI Baudrate Factor
	SPI1_BR &= ~SPI_BR_SPPR_MASK; SPI1_BR |= SPI_BR_SPPR(2);
	SPI1_BR &= ~SPI_BR_SPR_MASK;  SPI1_BR |= SPI_BR_SPR(4);
	// Enable SPI
	SPI1_C1 |= (1U << SPI_C1_SPE_SHIFT);
}

uint8_t spi1_write(uint8_t data)
{
	while (!(SPI1_S & SPI_S_SPTEF_MASK)) { ; }
	SPI1_D = data;
  while (!(SPI1_S & SPI_S_SPRF_MASK)) { ; }
	return SPI1_D;
}
