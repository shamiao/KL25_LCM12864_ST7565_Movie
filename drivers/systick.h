/*
 * MKL25Z4 SysTick Driver
 *
 * Copyright (c) 2015 SHA Miao
 * This program is licensed under [The MIT License](http://opensource.org/licenses/MIT).
 */

#ifndef SYSTICK_H_
#define SYSTICK_H_

void systick_init();

void systick_enable();

void systick_disable();

#endif /* SYSTICK_H_ */
