/*
 * MKL25Z4 SPI0 Driver
 *
 * Copyright (c) 2015 SHA Miao
 * This program is licensed under [The MIT License](http://opensource.org/licenses/MIT).
 */

#ifndef SPI0_H_
#define SPI0_H_

#include <stdint.h>

void spi0_init();

void spi0_write(uint8_t data);

#endif /* SPI0_H_ */
