/*
 * MKL25Z4 GPIO Driver (Arduino Style)
 *
 * Copyright (c) 2015 SHA Miao
 * This program is licensed under [The MIT License](http://opensource.org/licenses/MIT).
 */

#include "MKL25Z4.h"
#include "gpio.h"

#define GPIO_PORT_PIN_INVALID (port>=5 || pin>=32)

const PORT_MemMapPtr gpio_reg_port_base[] = PORT_BASE_PTRS;
const GPIO_MemMapPtr gpio_reg_gpio_base[] = GPIO_BASE_PTRS;

void pinMux(uint8_t port, uint8_t pin, uint8_t mux)
{
  if (GPIO_PORT_PIN_INVALID || mux>=8) { return; }
  gpio_reg_port_base[port]->PCR[pin] &= ~PORT_PCR_MUX_MASK;
  gpio_reg_port_base[port]->PCR[pin] |= PORT_PCR_MUX(mux);;
  return;
}

void pinMode(uint8_t port, uint8_t pin, uint8_t mode)
{
  if (GPIO_PORT_PIN_INVALID) { return; }
  if (mode == INPUT) {
    GPIO_PDDR_REG(gpio_reg_gpio_base[port]) &= GPIO_PDDR_PDD(~((uint32_t)(1UL) << pin));
  } else {
    GPIO_PDDR_REG(gpio_reg_gpio_base[port]) |= GPIO_PDDR_PDD((uint32_t)(1UL) << pin);
  }
}

void digitalWrite(uint8_t port, uint8_t pin, uint8_t value)
{
  if (GPIO_PORT_PIN_INVALID) { return; }
  if (value == LOW) {
    GPIO_PCOR_REG(gpio_reg_gpio_base[port]) = GPIO_PCOR_PTCO((uint32_t)(1UL) << pin);
  } else {
    GPIO_PSOR_REG(gpio_reg_gpio_base[port]) = GPIO_PSOR_PTSO((uint32_t)(1UL) << pin);
  }
}

int8_t digitalRead(uint8_t port, uint8_t pin)
{
  if (GPIO_PORT_PIN_INVALID) { return -1; }
  return (GPIO_PDIR_REG(gpio_reg_gpio_base[port]) & GPIO_PDIR_PDI((uint32_t)(1UL) << pin))
    ? HIGH : LOW;
}

void digitalFlip(uint8_t port, uint8_t pin)
{
  if (GPIO_PORT_PIN_INVALID) { return; }
  GPIO_PTOR_REG(gpio_reg_gpio_base[port]) = GPIO_PTOR_PTTO((uint32_t)(1UL) << pin);
}
