/*
 * MKL25Z4 SPI0 Driver
 *
 * Copyright (c) 2015 SHA Miao
 * This program is licensed under [The MIT License](http://opensource.org/licenses/MIT).
 */

#include "MKL25Z4.h"
#include "gpio.h"
#include "spi0.h"

void spi0_init()
{
	// Enable SPI Clock
	SIM_SCGC4 |= (1U << SIM_SCGC4_SPI0_SHIFT);
	// PTA14 = ALT2: SPI0_PCS0
	pinMux(0, 14, MUX2_COMM);
	// PTA15 = ALT2: SPI0_SCK
  pinMux(0, 15, MUX2_COMM);
	// PTA16 = ALT2: SPI0_MOSI
  pinMux(0, 16, MUX2_COMM);
	// PTA17 = ALT2: SPI0_MISO
  pinMux(0, 17, MUX2_COMM);
	// SPI Settings: Master, CPOL=1 CPHA=1 (SPI Mode 3), LSBFE=0 (MSB First)
	SPI0_C1 |= (1U << SPI_C1_MSTR_SHIFT);
	SPI0_C1 |= (1U << SPI_C1_CPOL_SHIFT);
	SPI0_C1 |= (1U << SPI_C1_CPHA_SHIFT);
	SPI0_C1 &= ~SPI_C1_LSBFE_MASK;
	// SPI Settings: /SS Port as /CS Output
	SPI0_C1 |= (1U << SPI_C1_SSOE_SHIFT);
	SPI0_C2 |= (1U << SPI_C2_MODFEN_SHIFT);
	// SPI Baudrate Factor
  // ( Bus clock=24MHz * SPPR(3)=1/4 * SPR(0)=1/2 ) = 3MHz
	SPI0_BR &= ~SPI_BR_SPPR_MASK; SPI0_BR |= SPI_BR_SPPR(3);
	SPI0_BR &= ~SPI_BR_SPR_MASK;  SPI0_BR |= SPI_BR_SPR(0);
	// Enable SPI
	SPI0_C1 |= (1U << SPI_C1_SPE_SHIFT);
}

void spi0_write(uint8_t data)
{
	while (!(SPI0_S & SPI_S_SPTEF_MASK)) { ; }
	SPI0_D = data;
	return;
}
