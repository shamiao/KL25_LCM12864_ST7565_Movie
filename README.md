Freescale KL25平台ST7565液晶模块视频播放
========================================

使用Freescale的KL25系列Cortex M0+微控制器，在常见的以ST7565为主控的12864 COG液晶模块上播放黑白视频。

** 视频演示请见：<http://v.youku.com/v_show/id_XODkyNDk4Njky.html> **

硬件平台
----------
1. 苏州大学飞思卡尔嵌入式中心 SD-FSL-KL25-EVE 评估板
2. ST7565 12864 COG液晶模块
3. SD卡驱动模块
4. 任意SDHC存储卡

开发环境
----------
Kinetis Design Studio IDE v2.0.0 (内置 Cross ARM GCC 工具链)

电路连接
----------

### 液晶模块

`/CS`-`A14`, `/RST`-`A13`, `/A0`-`A12`, `/SCK`-`A15`, `/SDI`-`A16`

### SD卡模块

`/CS(DAT3)`-`B10`, `DI(CMD)`-`B16`, `DO(DAT0)`-`B17`, `CLK(SCLK)`-`B11`

### 其他

`B18`-CPU占用率输出（共享评估板的绿色LED）

系统组件
----------

 - `/drivers/systick.*`: 系统时基
 - `/drivers/spi0.*`: SPI0驱动（用于液晶模块）
 - `/drivers/spi1.*`: SPI1驱动（用于SD卡）
 - `/drivers/gpio.*`: GPIO驱动（Arduino风格）
 - `/components/lcm12864_st7565.*`: 液晶模块组件
 - `/components/sdhccard.*`: SDHC存储卡组件

使用
----------
1. 本软件包未实现文件系统与视频解码。视频按照每一帧图像液晶取模的原始数据（1024字节每帧）顺序排列后，直接写入存储卡的开头。
2. 软件随附一个演示视频`/dist/badapple.img.gz`。请解压缩后将`badapple.img`用`dd` (UNIX)或[USB Image Tool](http://www.alexpage.de/usb-image-tool/) (Windows)等工具直接写盘。
3. 烧写文件请参阅`/dist/KL25_LCM12864_ST7565_Movie.*`。
4. 播放程序上电即自动运行。

注意
----------
代码中多有临时手段或处理不周之处，请多加留意。应用中请注意按阁下个人需求修改。

仅支持SDHC存储卡（一般为4GB或以上容量），不支持不带SDHC标志的旧SD卡。

许可
----------
本仓库以[MIT许可证](http://opensource.org/licenses/MIT)许可。

> 简而言之：您可以任意使用本仓库的代码，惟须保留`LICENSE`（许可证文本）和`CONTRIBUTORS`（贡献者列表）两个文件（或将文件内容登载到其他合适位置）。具体请参阅许可证文本的说明。

本仓库包含[苏州大学飞思卡尔嵌入式心中心](http://sumcu.suda.edu.cn/)授权任意使用的代码。

本仓库包含[Freescale Semiconductor](http://www.freescale.com/)提供的硬件相关头文件。
